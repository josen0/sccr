/*
	SCCR v1.1

	Copyright (C) 2020 J. Nieto, all rights reserved.
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
   	the Free Software Foundation; either version 3, or (at your option)
   	any later version.

   	This program is distributed in the hope that it will be useful,
   	but WITHOUT ANY WARRANTY; without even the implied warranty of
   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	GNU General Public License for more details.

 	You should have received a copy of the GNU General Public License
    	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	contact: kkeezff@hotmail.com

*/

#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

//#define DEMO

//#define DEBUG

#define	NUM_LINE_MAX    10
#define ZERO_LINE_MAX   {0,0,0,0,0,0,0,0,0,0}

#define BAR_PROGRESS    '|'

enum {isANSI, isUTF8, isUTF16LE, isUTF16BE};

int _tmain(int argc, _TCHAR* argv[])
{

	size_t i, opc, line_number;
	FILE *fin, *fout, *fout_c;

	char line_number_s[NUM_LINE_MAX] = ZERO_LINE_MAX;

	char clean_c[255] = "~";

	bool onlySOLIDUS = true, onlyASTERISK = true;

	wchar_t c_w, tmp_w;
	char16_t tmp;
	char c;

	printf("C-Style Comment Cleaner v1.1.0\n");

	if(argc < 2){
		printf("Usage: cscc [filename]");
	}

	fin = fopen(argv[1],"rb");
	if(fin == NULL)
	{
		printf("Error opening source file.\n");
		exit(EXIT_FAILURE);
	}

	char16_t    header;

	if((header = fgetwc(fin)) != WEOF)
		switch(header)
		{
			case Header_UTF_16BE:
				opc = isUTF16BE;
				printf("UTF-16BE Byte Order Mark (BOM) detected!\n");
				break;

			case Header_UTF_16LE:
				opc = isUTF16LE;
				printf("UTF-16LE Byte Order Mark (BOM) detected!\n");
				break;

			default:
				opc = isANSI;
				printf("ANSI detected!\n");
				break;
		}

	if(argc == 3)
	{
		if (strcmp(argv[2],"-/") == 0)
		{
			printf("Only // comments will be removed.\n");
			onlyASTERISK = false;

		}

		if (strcmp(argv[2],"-*") == 0)
		{
			printf("Only /**/ comments will be removed.\n");
			onlySOLIDUS = false;
		}
	}

	line_number = 1;

	printf("Processing file...\n");
	fout = fopen(strcat(clean_c,argv[1]),"wb");
	fout_c = fopen(strcat(clean_c,".TXT"),"wb");

	switch(opc)
	{

/********************************
**  UTF 16 LE
********************************/

		case isUTF16LE:

			if(fout == NULL)
			{
				printf("Error opening clean-comment source file.\n");
				exit(EXIT_FAILURE);
			}

			if(fout_c == NULL)
			{
				printf("Error opening comment file.\n");
				exit(EXIT_FAILURE);
			}


				#ifdef DEBUG
				printf("[LE]Imprimiendo cabecera del archivo.\n");
				#endif
				fputwc(header,fout);
				fputwc(header,fout_c);

				#ifdef DEMO
				while((c_w = fgetwc(fin)) != WEOF && line_number < 100)
				#else
				while((c_w = fgetwc(fin)) != WEOF)
				#endif
				{
					#ifdef DEBUG
					printf("[LE]Entrando al loop de lectura del archivo.\n");
					#endif

					putchar(BAR_PROGRESS);

					if(c_w == SOLIDUS_UTF_16LE)
					{
						#ifdef DEBUG
						printf("[LE]-SOLIDUS- encontrado.\n");
						#endif

						c_w = fgetwc(fin);
						putchar(BAR_PROGRESS);

						switch(c_w)
						{
							case SOLIDUS_UTF_16LE:

								#ifdef DEBUG
								printf("[LE]Segundo -SOLIDUS- encontrado.\n");
								#endif

								if (onlySOLIDUS == true)
								{

									itoa(line_number, line_number_s, 10);

									for(i = 0; i < NUM_LINE_MAX; i++)
									{
										tmp = 0x00FF & line_number_s[i];
										fputwc(tmp,fout_c);
									}

									fputwc(TABULATION_UTF_16LE,fout_c);
									fputwc(SOLIDUS_UTF_16LE,fout_c);
									fputwc(c_w,fout_c);
								}
								else
								{
									fputwc(SOLIDUS_UTF_16LE,fout);
									fputwc(c_w,fout);
                                    break;
								}

								#ifdef DEBUG
								printf("[LE]Se escriben los dos -SOLIDUS- en el archivo.\n");
								printf("[LE]Entrando al loop de copia de comentarios.\n");
								#endif

								while((c_w = fgetwc(fin)) != WEOF)
								{
									putchar(BAR_PROGRESS);

									fputwc(c_w,fout_c);

									#ifdef DEBUG
									printf("[LE]Se escribio el caracter en el archivo de comentarios.\n");
									#endif

									if(c_w == CR_UTF_16LE)
									{
										c_w = fgetwc(fin);
                                        putchar(BAR_PROGRESS);
										fputwc(c_w,fout_c);
										if( c_w == LF_UTF_16LE)
										{
											++line_number;

											#ifdef DEBUG
											printf("[LE]Retorno de Carro encontrado, # Linea: %d.\n", line_number);
											#endif

											fputwc(CR_UTF_16LE,fout);
											fputwc(LF_UTF_16LE,fout);

                                            //fin de comentario tipo linea
											break;

										}

									}

								}

								break;

							case ASTERISK_UTF_16LE:

								#ifdef DEBUG
								printf("[LE]-ASTERISK- encontrado.\n");
								#endif

								if (onlyASTERISK == true)
								{

									itoa(line_number, line_number_s, 10);

									for(i = 0; i < NUM_LINE_MAX; i++)
									{

										tmp = 0x00FF & line_number_s[i];

										fputwc(tmp,fout_c);

									}

									fputwc(TABULATION_UTF_16LE,fout_c);
									fputwc(SOLIDUS_UTF_16LE,fout_c);
									fputwc(c_w,fout_c);
								}
								else
								{

									fputwc(SOLIDUS_UTF_16LE,fout);
									fputwc(c_w,fout);
									break;

								}


								#ifdef DEBUG
								printf("[LE]Se escriben el -SOLIDUS- y -ASTERISK- en el archivo.\n");
								printf("[LE]Entrando al loop de copia de comentarios.\n");
								#endif

								while((c_w = fgetwc(fin)) != WEOF)
								{

									putchar(BAR_PROGRESS);

									fputwc(c_w,fout_c);

									#ifdef DEBUG
									printf("[LE]Se escribio el caracter en el archivo de comentarios.\n");
									#endif

									if(c_w == CR_UTF_16LE)
									{
										c_w = fgetwc(fin);
										putchar(BAR_PROGRESS);
										fputwc(c_w,fout_c);

										if(c_w == LF_UTF_16LE)
										{
											++line_number;

											#ifdef DEBUG
											printf("[LE]Retorno de Carro encontrado, # Linea: %d.\n", line_number);
											#endif

										}


									}

									if(c_w == ASTERISK_UTF_16LE)
									{

										c_w = fgetwc(fin);
										putchar(BAR_PROGRESS);
										fputwc(c_w,fout_c);

										if( c_w == SOLIDUS_UTF_16LE)
										{

											#ifdef DEBUG
											printf("[LE]-ASTERISK- y -SOLIDUS- encontrados en pareja, se escriben ambos caracteres en el archivo comentario.\n");
											#endif

											fputwc(CR_UTF_16LE,fout_c);
											fputwc(LF_UTF_16LE,fout_c);

											break;
										}

									}
								}

								break;

							default:
								fputwc(SOLIDUS_UTF_16LE,fout);

								fputwc(c_w,fout);

								#ifdef DEBUG
								printf("[LE]Se escribio el caracter en el archivo de comentarios.\n");
								#endif

								if(c_w == CR_UTF_16LE)
								{
									c_w = fgetwc(fin);
                                    putchar(BAR_PROGRESS);
									fputwc(c_w,fout);
									if(c_w == LF_UTF_16LE)
									{
										++line_number;

										#ifdef DEBUG
										printf("[LE]Retorno de Carro encontrado, # Linea: %d.\n", line_number);
										#endif

									}

								}

								break;
						}
					}
					else
					{
						fputwc(c_w,fout);

						#ifdef DEBUG
						printf("[LE]Se escribio el caracter en el archivo de comentarios.\n");
						#endif

						if(c_w == CR_UTF_16LE)
						{
							c_w = fgetwc(fin);
							putchar(BAR_PROGRESS);
							fputwc(c_w,fout);
							if( c_w == LF_UTF_16LE)
							{
								++line_number;

								#ifdef DEBUG
								printf("[LE]Retorno de Carro encontrado, # Linea: %d.\n", line_number);
								#endif

							}

						}


					}
				}

				fclose(fout_c);
				fclose(fout);
				fclose(fin);

				printf("\nDone! :)\n");

				exit(EXIT_SUCCESS);

			break;

/********************************
**  UTF 16 BE
********************************/

		case isUTF16BE:

			if(fout == NULL)
			{
				printf("Error opening clean-comment source file.\n");
				exit(EXIT_FAILURE);
			}

			if(fout_c == NULL)
			{
				printf("Error opening comment file.\n");
				exit(EXIT_FAILURE);
			}

				#ifdef DEBUG
				printf("[BE]Imprimiendo cabecera del archivo.\n");
				#endif
				fputwc(header,fout);
				fputwc(header,fout_c);

				#ifdef DEMO
				while((c = fgetwc(fin)) != WEOF && line_number < 100)
				#else
				while((c_w = fgetwc(fin)) != WEOF)
				#endif
				{
				#ifdef DEBUG
				printf("[BE]Entrando al loop de lectura del archivo.\n");
				#endif
				putchar(BAR_PROGRESS);

					if(c_w == SOLIDUS_UTF_16BE)
					{
						#ifdef DEBUG
						printf("[BE]-SOLIDUS- encontrado.\n");
						#endif

						c_w = fgetwc(fin);
						putchar(BAR_PROGRESS);

						switch(c_w)
						{
							case SOLIDUS_UTF_16BE:

								#ifdef DEBUG
								printf("[BE]Segundo -SOLIDUS- encontrado.\n");
								#endif

								if (onlySOLIDUS == true)
								{

									itoa(line_number, line_number_s, 10);

									for(i = 0; i < NUM_LINE_MAX; i++)
									{
										tmp = 0x00FF & line_number_s[i];

										tmp <<= 8;

										tmp &= 0xFF00;

										fputwc(tmp,fout_c);
									}

									fputwc(TABULATION_UTF_16BE,fout_c);
									fputwc(SOLIDUS_UTF_16BE,fout_c);
									fputwc(c_w,fout_c);
								}
								else
								{
									fputwc(SOLIDUS_UTF_16BE,fout);
									fputwc(c_w,fout);
									break;
								}

								#ifdef DEBUG
								printf("[BE]Se escriben los dos -SOLIDUS- en el archivo.\n");
								#endif

								#ifdef DEBUG
								printf("[BE]Entrando al loop de copia de comentarios.\n");
								#endif

								while((c_w = fgetwc(fin)) != WEOF)
								{
									putchar(BAR_PROGRESS);

									fputwc(c_w,fout_c);

									#ifdef DEBUG
									printf("[BE]Se escribio el caracter en el archivo de comentarios.\n");
									#endif

									if(c_w == CR_UTF_16BE)
									{
										c_w = fgetwc(fin);
										putchar(BAR_PROGRESS);
										fputwc(c_w,fout_c);
										if( c_w == LF_UTF_16BE)
										{
											++line_number;

											#ifdef DEBUG
											printf("[BE]Retorno de Carro encontrado, # Linea: %d.\n", line_number);
											#endif

											fputwc(CR_UTF_16BE,fout);
											fputwc(LF_UTF_16BE,fout);

											//fin de comentario tipo linea
											break;

										}

									}

								}

								putchar(BAR_PROGRESS);

								break;

							case ASTERISK_UTF_16BE:

								#ifdef DEBUG
								printf("[BE]-ASTERISK- encontrado.\n");
								#endif

								if (onlyASTERISK == true)
								{

									itoa(line_number, line_number_s, 10);

									for(i = 0; i < NUM_LINE_MAX; i++)
									{

										tmp = 0x00FF & line_number_s[i];

										tmp <<= 8;

										tmp &= 0xFF00;

										fputwc(tmp,fout_c);

									}

									fputwc(TABULATION_UTF_16BE,fout_c);
									fputwc(SOLIDUS_UTF_16BE,fout_c);
									fputwc(c_w,fout_c);
								}
								else
								{

									fputwc(SOLIDUS_UTF_16BE,fout);
									fputwc(c_w,fout);
									break;

								}


								#ifdef DEBUG
								printf("[BE]Se escriben el -SOLIDUS- y -ASTERISK- en el archivo.\n");
								printf("[BE]Entrando al loop de copia de comentarios.\n");
								#endif

								while((c_w = fgetwc(fin)) != WEOF)
								{

									putchar(BAR_PROGRESS);

									fputwc(c_w,fout_c);

									#ifdef DEBUG
									printf("[BE]Se escribio el caracter en el archivo de comentarios.\n");
									#endif

									if(c_w == CR_UTF_16BE)
									{
										c_w = fgetwc(fin);
										putchar(BAR_PROGRESS);
										fputwc(c_w,fout_c);

										if(c_w == LF_UTF_16BE)
										{
											++line_number;

											#ifdef DEBUG
											printf("[BE]Retorno de Carro encontrado, # Linea: %d.\n", line_number);
											#endif

										}


									}

									if(c_w == ASTERISK_UTF_16BE)
									{

										c_w = fgetwc(fin);
										putchar(BAR_PROGRESS);
										fputwc(c_w,fout_c);

										if( c_w == SOLIDUS_UTF_16BE)
										{

											#ifdef DEBUG
											printf("[BE]-ASTERISK- y -SOLIDUS- encontrados en pareja, se escriben ambos caracteres en el archivo comentario.\n");
											#endif

											fputwc(CR_UTF_16BE,fout_c);
											fputwc(LF_UTF_16BE,fout_c);

											break;
										}

									}
								}

								break;

							default:
								fputwc(SOLIDUS_UTF_16BE,fout);

								fputwc(c_w,fout);

								#ifdef DEBUG
								printf("[BE]Se escribio el caracter en el archivo de comentarios.\n");
								#endif

								if(c_w == CR_UTF_16BE)
								{
									c_w = fgetwc(fin);
									putchar(BAR_PROGRESS);
									fputwc(c_w,fout);
									if(c_w == LF_UTF_16BE)
									{
										++line_number;

										#ifdef DEBUG
										printf("[BE]Retorno de Carro encontrado, # Linea: %d.\n", line_number);
										#endif

									}

								}

								break;
						}
					}
					else
					{
						fputwc(c_w,fout);

						#ifdef DEBUG
						printf("[BE]Se escribio el caracter en el archivo de comentarios.\n");
						#endif

						if(c_w == CR_UTF_16BE)
						{
							c_w = fgetwc(fin);
							putchar(BAR_PROGRESS);
							fputwc(c_w,fout);
							if( c_w == LF_UTF_16BE)
							{
								++line_number;

								#ifdef DEBUG
								printf("[BE]Retorno de Carro encontrado, # Linea: %d.\n", line_number);
								#endif

							}

						}


					}
				}

				fclose(fout_c);
				fclose(fout);
				fclose(fin);

				printf("\nDone! :)\n");

				exit(EXIT_SUCCESS);

			break;


/********************************
**  UTF 8
**  ANSI
********************************/

		default:

			if(fout == NULL)
			{
				printf("Error opening clean-comment source file.\n");
				exit(EXIT_FAILURE);
			}

			if(fout_c == NULL)
			{
				printf("Error opening comment file.\n");
				exit(EXIT_FAILURE);
			}


			#ifdef DEBUG
			printf("[ANSI]Imprimiendo cabecera del archivo.\n");
			#endif
			fputwc(header,fout);
			fputwc(header,fout_c);

			#ifdef DEMO
			while((c = fgetc(fin)) != EOF && line_number < 100)
			#else
			while((c = fgetc(fin)) != EOF)
			#endif
			{
				#ifdef DEBUG
				printf("[ANSI]Entrando al loop de lectura del archivo.\n");
				#endif

				putchar(BAR_PROGRESS);

				if(c == SOLIDUS_ANSI)
				{
					#ifdef  DEBUG
					printf("[ANSI]-SOLIDUS- encontrado.\n");
					#endif

					c = fgetc(fin);
					putchar(BAR_PROGRESS);

					switch(c)
					{
						case SOLIDUS_ANSI:

							#ifdef DEBUG
							printf("[ANSI]Segundo -SOLIDUS- encontrado.\n");
							#endif

							if(onlySOLIDUS == true)
							{

								itoa(line_number, line_number_s,10);

								for(i = 0; i < NUM_LINE_MAX; i++)
								{
									fputc(line_number_s[i],fout_c);
								}

								fputc(TABULATION_ANSI,fout_c);
								fputc(SOLIDUS_ANSI,fout_c);
								fputc(c,fout_c);

							}
							else
							{
								fputc(SOLIDUS_ANSI,fout);
								fputc(c,fout);
								break;
							}

							#ifdef  DEBUG
							printf("[ANSI]Se escriben los dos -SOLIDUS- en el archivo.\n");
							printf("[ANSI]Entrando al loop de copia de comentarios.\n");
							#endif

							while((c = fgetc(fin)) != EOF)
							{
								putchar(BAR_PROGRESS);

								fputc(c,fout_c);

								#ifdef  DEBUG
								printf("[ANSI]Se escribio el caracter en el archivo de comentarios.\n");
								#endif

								if(c == '\n')
								{
									++line_number;

									#ifdef  DEBUG
									printf("[ANSI]Retorno de Carro encontrado, # Linea: %d.\n", line_number);
									#endif

									fputc(c,fout);

									break;
								}
							}

							break;

						case ASTERISK_ANSI:

							#ifdef  DEBUG
							printf("[ANSI]-ASTERISK- encontrado.\n");
							#endif

							if(onlyASTERISK == true)
							{
								itoa(line_number, line_number_s, 10);

								for(i = 0; i < NUM_LINE_MAX; i++)
								{
									fputc(line_number_s[i],fout_c);
								}

								fputc(TABULATION_ANSI,fout_c);
								fputc(SOLIDUS_ANSI,fout_c);
								fputc(c,fout_c);

							}
							else
							{

								fputc(SOLIDUS_ANSI,fout);
								fputc(c,fout);
								break;

							}

							#ifdef  DEBUG
							printf("[ANSI]Se escriben el -SOLIDUS- y -ASTERISK- en el archivo.\n");
							printf("[ANSI]Entrando al loop de copia de comentarios.\n");
							#endif


							while((c = fgetc(fin)) != EOF)
							{

								putchar(BAR_PROGRESS);

								fputc(c,fout_c);

								#ifdef  DEBUG
								printf("[ANSI]Se escribio el caracter en el archivo de comentarios.\n");
								#endif

								if(c == '\n')
								{

									++line_number;
									#ifdef  DEBUG
									printf("[ANSI]Retorno de Carro encontrado, # Linea: %d.\n",line_number);
									#endif

								}


								if(c == ASTERISK_ANSI)
								{
									c = fgetc(fin);
									putchar(BAR_PROGRESS);
									fputc(c,fout_c);

									if(c == SOLIDUS_ANSI)
									{

										#ifdef  DEBUG
										printf("[ANSI]-ASTERISK- y -SOLIDUS- encontrados en pareja, se escriben ambos caracteres en el archivo comentario.\n");
										#endif

										fputc('\n',fout_c);

										break;
									}
								}

							}

							break;

						default:
							fputc(SOLIDUS_ANSI,fout);

							fputc(c,fout);

							#ifdef  DEBUG
							printf("[ANSI]Se escribio el caracter en el archivo de comentarios.\n");
							#endif

							if(c == '\n')
							{

								++line_number;

								#ifdef  DEBUG
								printf("[ANSI]Retorno de Carro encontrado, # Linea: %d.\n", line_number);
								#endif

							}

							break;
					}
				}
				else
				{
					fputc(c,fout);

					#ifdef  DEBUG
                    printf("[ANSI]Se escribio el caracter en el archivo de comentarios.\n");
					#endif

					if(c == '\n')
					{
						++line_number;

						#ifdef  DEBUG
						printf("[ANSI]Retorno de Carro encontrado, # Linea: %d.\n");
						#endif
					}

				}


			}

				fclose(fout_c);
				fclose(fout);
				fclose(fin);

				printf("\nDone! :)\n");

				exit(EXIT_SUCCESS);

				break;

	}

return 0;
}


