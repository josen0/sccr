# SCCR

## ¿Qué es SCCR?

SCCR es una utilidad que publiqué en itch.io pensando que seria de utilidad para alguien, por este programa pedía una donación de $1 USD, fue publicado desde el 2020 pero no tuve ninguna compra y el interés fue nulo. Por tal motivo lo libero como software libre.


A continuación, pongo la pequeña documentación que estaba en itch.io.

			-= SCCR =-
		Source Code Comment Remover
			  v1.1.0

SCCR es un software que le permite remover los comentarios a sus archivos
fuente.


>>	* Características *
	-----------------------------------------------------------------------

En esta versión solo soporta comentarios estilo C/C++ (// y /**/).

Puede limpiar comentarios tipo // y /**/ o por separado.

Los comentarios son transferidos a un archivo de texto con numero de línea.

Compatible solo con Windows.


>>	* Uso *
	-----------------------------------------------------------------------

sccr [archivo-fuente] [-/] [-*]

-/	Remueve todos los comentarios //

-*	Remueve todos los comentarios /**/

Sin parámetros, remueve ambos

SCCR devuelve los siguientes archivos:

~[archivo-fuente]	Contiene el archivo fuente sin los comentarios.
~[archivo-fuente].TXT	Contiene los comentarios removidos del archivo fuente.
